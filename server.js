var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var path = require('path');
var requestjson = require('request-json')
var bodyparser = require('body-parser'); //para enviar parámetros de forma segura en el body

app.use(bodyparser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // que no marque CORS
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, PUT");
  next();
})
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/cramirezs/collections/";
var apiKey = "?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

//--------------------EMPIEZAN PETICIONES HTTP------------------------//
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/signin', function(req, res) {
  /*
   *req.body: json de email y password
   *IF body en usuarios: logueado; res status 202
   *ELSE: no encontrado: res status 404
   */
  query = 'q={"email": "'+ req.body.email +'", "password": "'+ req.body.password +'"}'
  clienteMlab = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)

  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      if(body.length == 1) {
        res.status(200).send("Usuario logueado")
      } else {
        res.status(404).send("Usuario no encontrado")
      }
    }
  })
})

app.post('/signup', function(req, res) {
  /*
   *req.body: json de email y password
   *IF req.body en usuarios: usuario ya existe; res status 202
   *ELSE: registra usuario con email, password y movimientos; res body
   */
  query = 'q={"email": "'+ req.body.email +'", "password": "'+ req.body.password +'"}'
  clienteMlab = requestjson.createClient(urlMlabRaiz + "Usuarios" + apiKey + "&" + query)

  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      if(body.length == 1) {
        res.status(200).send("El usuario ya existe")
      } else {
        nuevoUsuario = '{"email": "' + req.body.email +'", "password": "' + req.body.password +'", "movimientos": []' + '}'
        clienteMlab.post('', JSON.parse(nuevoUsuario), function(err, resM, body) {
          res.send(body)
        })
      }
    }
  })
})

app.get('/Movimientos/:id', function(req, res) {
  /*
   *req.params: id de usuario
   *IF encuentra id:
   *---IF body.movimientos vacío: no hay movimientos
   *---ELSE: res body.movimientos
   *ELSE: imprime res body en consola
   */
  clienteMlab = requestjson.createClient(urlMlabRaiz + "Usuarios" + "/" + req.params.id + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      body.movimientos.length == 0 ? res.send("No tienes movimientos") : res.send(body.movimientos)
    } else {
      console.log(body);
    }
  })
})

app.put('/Movimientos/:id', function(req, res){
  /*
   *req.params: id de usuario
   *IF encuentra id: push movimiento indicado en req.body, res body
   *ELSE: imprime res body en consola
   */
  //Retorma usuario al identificarlo mediante el _id
  clienteMlab = requestjson.createClient(urlMlabRaiz + "Usuarios" + "/" + req.params.id + apiKey)

  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      nuevoMovimiento = '{"$push": {"movimientos": '+ JSON.stringify(req.body) +'}}'
      clienteMlab.put('', JSON.parse(nuevoMovimiento), function(err, resM, body) {
        res.send(body)
      })
    } else {
      console.log(body);
    }
  })
})
